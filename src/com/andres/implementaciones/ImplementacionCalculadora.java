package com.andres.implementaciones;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.andres.constantes.Operacion;
import com.andres.interfaces.ServicioCalculadora;

public class ImplementacionCalculadora extends UnicastRemoteObject implements ServicioCalculadora {

	private static final long serialVersionUID = 1L;
	
	public ImplementacionCalculadora() throws RemoteException {
		super();
	}

	@Override
	public Integer operar(int operandoA, int operandoB, Operacion operacion) throws RemoteException {
		Integer respuesta = new Integer(0);
		
		switch (operacion) {
		case SUMA:
			respuesta = new Integer(operandoA + operandoB);
			break;
		case RESTA:
			respuesta = new Integer(operandoA - operandoB);
			break;
		case MUTIPLICACION:
			respuesta = new Integer(operandoA * operandoB);
			break;
		case DIVISION:
			respuesta = new Integer(operandoA / operandoB);
			break;
		case POTENCIA:
			respuesta = new Integer((int) Math.pow(operandoA, operandoB));
			break;
		case RESTO:
			respuesta = new Integer(operandoA % operandoB);
			break;
		}
		
		return respuesta;
	}


}
