package com.andres.servidorrmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import com.andres.implementaciones.ImplementacionCalculadora;

public class ServidorRMI {
		
	public static void main(String[] args){
		try {
			Naming.rebind("//localhost/MiServidor", new ImplementacionCalculadora());
			System.err.println("servidor iniciado!!");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
