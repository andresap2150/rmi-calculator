package com.andres.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.andres.constantes.Operacion;

public interface ServicioCalculadora extends Remote{
	Integer operar(int operandoA, int operandoB, Operacion operacion) throws RemoteException;
}
