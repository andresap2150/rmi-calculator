package com.andres.clientermi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

import com.andres.constantes.Operacion;
import com.andres.interfaces.ServicioCalculadora;

public class OperacionCliente {
	private static ServicioCalculadora Objeto_remoto;
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
		Objeto_remoto = (ServicioCalculadora) Naming.lookup("//localhost/MiServidor");
		
		int operadorA = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el operando A"));
		int operadorB = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el operando B"));
		Integer respuesta = Objeto_remoto.operar(operadorA, operadorB, Operacion.SUMA);
		JOptionPane.showMessageDialog(null, 
				"La respuesta del server es:"+respuesta.toString());
	}
}
